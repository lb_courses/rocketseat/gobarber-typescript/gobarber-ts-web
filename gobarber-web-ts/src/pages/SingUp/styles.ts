import styled, { keyframes } from 'styled-components';
import { shade } from 'polished';
import singUpBackGroundImg from '../../assets/sign-up-background.png';

export const Container = styled.div`
  /* 100% de altura */
  height: 100vh;
  display: flex;
  /* faz com que os itens do container tenham a altura máxima da page */
  align-items: stretch;
`;

// export const Content = styled.div`
//   /* padding: 50px; */
//   display: flex;
//   flex-direction: column;
//   /* centralina  */
//   align-items: center;

//   place-content: center;
//   /* justify-content: center;
//   align-items: center; */
//   width: 100%;
//   max-width: 700px;

//   form {
//     margin: 20px 0;
//     width: 340px;

//     text-align: center;

//     h1 {
//       margin-bottom: 24px;
//     }

//     a {
//       color: #f4ede8;
//       display: block;
//       margin-top: 24px;
//       text-decoration: none;
//       transition: color 0.2s;

//       &:hover {
//         color: ${shade(0.2, '#f4ede8')};
//       }
//     }
//   }

//   /* os 'a' que estão diretamente dentro da div */
//   > a {
//     color: #f4ede8;
//     display: block;
//     margin-top: 24px;
//     text-decoration: none;
//     transition: color 0.2s;

//     display: flex;
//     align-items: center;

//     svg {
//       margin-right: 16px;
//     }

//     &:hover {
//       color: ${shade(0.2, '#f4ede8')};
//     }
//   }
// `;

export const Backgrount = styled.div`
  /* acupa todo o espaço */
  flex: 1;
  background: url(${singUpBackGroundImg}) no-repeat center;
  /* ocupa toda a div, caso a img seja menor que ela */
  background-size: cover;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  /* centralina  */
  align-items: center;
  justify-content: center;

  /* place-content: center; */
  /* justify-content: center;
  align-items: center; */
  width: 100%;
  max-width: 700px;
`;

const apperFromRight = keyframes`
    from {
      opacity: 0;
      transform: translateX(50px);
    }

    to {
      opacity: 1;
      transform: translateX(0);
    }

  `;

export const AnimationContainer = styled.div`
  /* padding: 50px; */
  display: flex;
  flex-direction: column;
  /* centralina  */
  align-items: center;
  justify-content: center;

  animation: ${apperFromRight} 1s;

  form {
    margin: 20px 0;
    width: 340px;

    text-align: center;

    h1 {
      margin-bottom: 24px;
    }

    a {
      color: #f4ede8;
      display: block;
      margin-top: 24px;
      text-decoration: none;
      transition: color 0.2s;

      &:hover {
        color: ${shade(0.2, '#f4ede8')};
      }
    }
  }

  /* os 'a' que estão diretamente dentro da div */
  > a {
    color: #ff9000;
    display: block;
    margin-top: 24px;
    text-decoration: none;
    transition: color 0.2s;

    display: flex;
    align-items: center;

    svg {
      margin-right: 16px;
    }

    &:hover {
      color: ${shade(0.2, '#ff9000')};
    }
  }
`;
