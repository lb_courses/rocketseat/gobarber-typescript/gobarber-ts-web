import styled from 'styled-components';

export const Container = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  padding: 30px;
  /* oculta o conteédo que exeder o conteiner */
  overflow: hidden;
`;
