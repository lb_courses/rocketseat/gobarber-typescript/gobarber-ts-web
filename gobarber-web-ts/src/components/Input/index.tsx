import React, {
  InputHTMLAttributes,
  useEffect,
  // registra o component para o form monitorar
  useRef,
  useState,
  // cria a função apenas uma vez
  useCallback,
} from 'react';
import { IconBaseProps } from 'react-icons';
import { FiAlertCircle } from 'react-icons/fi';
import { useField } from '@unform/core';
import { Container, Error } from './styles';

// pega todas as props do input e deixa o name obrigatório
interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  // recebe um component como propriedade
  icon?: React.ComponentType<IconBaseProps>;
  // eslint-disable-next-line @typescript-eslint/ban-types
  containerStyle?: object;
}

// converte com a primeira letra maiúscula para o react entender que é um component
const Input: React.FC<InputProps> = ({
  name,
  containerStyle,
  icon: Icon,
  ...rest
}) => {
  const { fieldName, defaultValue, error, registerField } = useField(name);
  const inputRef = useRef<HTMLInputElement>(null);
  const [isFocused, setIsFocused] = useState(false);
  const [isFielled, setIsFielled] = useState(false);

  const handleInputFocus = useCallback(() => {
    setIsFocused(true);
  }, []);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);

    setIsFielled(!!inputRef.current?.value);
  }, []);

  useEffect(() => {
    registerField({
      name: fieldName,
      // ref, da acesso ao input no DOM
      ref: inputRef.current,
      // valor do input
      path: 'value',
    });
  }, [fieldName, registerField]);

  return (
    <Container
      style={containerStyle}
      isErrored={!!error}
      isFocused={isFocused}
      isFielled={isFielled}
    >
      {Icon && <Icon size={20} />}
      <input
        name={name}
        onFocus={() => handleInputFocus()}
        onBlur={() => handleInputBlur()}
        defaultValue={defaultValue}
        ref={inputRef}
        {...rest}
      />
      {error && (
        <Error title={error}>
          <FiAlertCircle color="#c53030" size="20" />
        </Error>
      )}
    </Container>
  );
};

export default Input;
